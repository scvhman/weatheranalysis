package com.crank.location;

import com.crank.location.units.City;
import com.crank.location.units.Coordinates;
import com.crank.location.units.Country;
import com.crank.location.units.Location;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.HashSet;

/**
 * Класс служит для превращения объектов класса Location в Coordinates и наоборот
 * Работает с nominatim api
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class Reverser {

    /**
     * Данный метод получает координаты с объектов
     *
     * @param location - локация превращаемая в координаты
     * @return set - ХЕШ СЕТ ВОЗВРАЩАЕТСЯ Т.К МОГУТ СУЩЕСТВОВАТЬ НЕСКОЛЬКО ЛОКАЦИЙ С ОДИНАКОВЫМИ ИМЕНАМИ
     */
    public HashSet<Coordinates> getCoordinates(Location location) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://nominatim.openstreetmap.org/search?country=" + location.getCountry().getName()
                + "&city=" + location.getCity().getName() + "&format=json&addressdetails=1").header("User-Agent", "crank.io").build();
        Response response = client.newCall(request).execute();
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonArray array = parser.parse(response.body().string()).getAsJsonArray();
        HashSet<Coordinates> set = new HashSet<>();
        Coordinates coordinates;
        //Были проблемы с токенами поэтому временно сделано таким простым циклом
        for (int i = 0; i < array.size(); i++) {
            coordinates = gson.fromJson(array.get(i), Coordinates.class);
            set.add(coordinates);
        }
        return set;
    }

    /** Данный метод получает Location из координат
     * @param coordinates - координаты превращаемые в локацию
     * @return location - возвращаемая локация
     */
    public Location getLocation(Coordinates coordinates) throws IOException {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("http://nominatim.openstreetmap.org/reverse?format=json&lat=" + coordinates.getLatitude() +
                "&lon=" + coordinates.getLongitude() + "&zoom=18&addressdetails=1").header("User-Agent", "crank.io").build();
        Response response = client.newCall(request).execute();
        Gson gson = new Gson();
        JsonParser parser = new JsonParser();
        JsonElement address = parser.parse(response.body().string()).getAsJsonObject().get("address");
        City city = gson.fromJson(address, City.class);
        Country country = gson.fromJson(address, Country.class);
        Location location = new Location(country, city);
        return location;
    }


}
