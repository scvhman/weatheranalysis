package com.crank.weather.units;

import com.google.gson.annotations.SerializedName;

/**
 * Класс служит для получения осстояний погоды
 * Десериализация производится с помощью GSON
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class Weather {

    @SerializedName(value = "temperature", alternate = {"temp", "dewpoint_f"})
    private double temperature;

    @SerializedName(value = "humidity")
    private double humidity;

    @SerializedName(value = "windSpeed", alternate = {"speed", "wind_mph"})
    private double windSpeed;

    @SerializedName(value = "visibility", alternate = {"visibility_mi"})
    private double visibility;

    @SerializedName("ozone")
    private double ozone;

    @SerializedName(value = "pressure", alternate = {"pressure_in"})
    private double pressure;

    @SerializedName(value = "summary", alternate = {"icon"})
    private String summary;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public double getVisibility() {
        return visibility;
    }

    public void setVisibility(double visibility) {
        this.visibility = visibility;
    }

    public double getOzone() {
        return ozone;
    }

    public void setOzone(double ozone) {
        this.ozone = ozone;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

}
