package com.crank.weather.weatherprovider;

import com.crank.location.units.Coordinates;
import com.crank.location.units.Location;
import com.crank.weather.units.Weather;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Интерфейс от которого наследуются все классы связанные с работой с какими-либо апи погоды
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public interface WeatherProvider {

    Weather getCurrentWeather(Coordinates coordinates) throws IOException;

    Weather getWeatherByTime(Coordinates coordinates, DateTime time) throws IOException;

    ArrayList<Weather> getCurrentWeather(Location location);

    ArrayList<Weather> getWeatherByTime(Location location, DateTime time);

    ArrayList<Weather> getWeekForecast(Coordinates coordinates);
}
