package com.crank.context;

import com.crank.compilationprovider.JSONProvider.JsonApiProvider;
import com.crank.compilationprovider.JSONProvider.JsonNowProvider;
import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.metricsystem.MetricTypes;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class JsonProvidersContext {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public JsonApiProvider nowStateUS() {
        return new JsonNowProvider(new MetricSystem(MetricTypes.US));
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public JsonApiProvider nowStateWorld() {
        return new JsonNowProvider(new MetricSystem(MetricTypes.World));
    }

}
