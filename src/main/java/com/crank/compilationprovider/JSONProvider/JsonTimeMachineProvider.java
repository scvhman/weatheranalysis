package com.crank.compilationprovider.JSONProvider;


import com.crank.context.WeatherProvidersContext;
import com.crank.location.units.Coordinates;
import com.crank.location.units.Location;
import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.metricsystem.MetricTypes;
import com.crank.weather.units.Weather;
import com.crank.weather.weatherprovider.DarkSkyApiProvider;
import org.joda.time.DateTime;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.ArrayList;

public class JsonTimeMachineProvider implements JsonApiProvider {

    private DarkSkyApiProvider darkSkyApiProvider;
    private ArrayList<Weather> compilation = new ArrayList<>();
    private DateTime time;

    public JsonTimeMachineProvider(MetricSystem metricSystem, DateTime time) {
        this.time = time;
        ApplicationContext providerContext =
                new AnnotationConfigApplicationContext(WeatherProvidersContext.class);
        if (metricSystem.getMetric() == MetricTypes.US) {
            darkSkyApiProvider = (DarkSkyApiProvider) providerContext.getBean("darkskyUS");
        } else {
            darkSkyApiProvider = (DarkSkyApiProvider) providerContext.getBean("darkskyWorld");
        }
    }

    @Override
    public ArrayList<Weather> getWeatherList(Coordinates coordinates) {
        try {
            Weather darkSkyWeather = darkSkyApiProvider.getWeatherByTime(coordinates, time);
            compilation.add(darkSkyWeather);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return compilation;
    }

    @Override
    public ArrayList<Weather> getWeatherList(Location location) {
        ArrayList<Weather> darkSkyWeathers = darkSkyApiProvider.getWeatherByTime(location, time);
        compilation.addAll(darkSkyWeathers);
        return compilation;
    }
}
