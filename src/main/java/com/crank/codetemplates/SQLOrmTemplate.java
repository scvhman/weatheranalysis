package com.crank.codetemplates;

import com.crank.weather.access.AccessToken;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class SQLOrmTemplate {

    public AccessToken getToken(String provider) {
        SessionFactory factory = new Configuration().configure().addAnnotatedClass(AccessToken.class)
                .buildSessionFactory();
        Session tokenReader = factory.openSession();
        Transaction tokenTransaction = tokenReader.beginTransaction();
        AccessToken token = tokenReader.get(AccessToken.class, provider);
        tokenTransaction.commit();
        return token;
    }
}
