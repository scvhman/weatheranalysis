package com.crank.weather.access;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Класс служит для хранения токенов доступа к различным API
 *
 * @author scvhabodka-man
 * @version 1.0
 */

@Entity
@Table(name = "tokens")
public class AccessToken {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AccessToken(String token) {
        this.token = token;
    }

    public AccessToken() {
    }

}
