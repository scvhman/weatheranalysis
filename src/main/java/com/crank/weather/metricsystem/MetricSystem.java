package com.crank.weather.metricsystem;

/**
 * Класс служит как оболочка для MetricTypes
 * Используется для получения данных в разных метрических системах
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class MetricSystem {

    private MetricTypes metric;

    public MetricSystem(MetricTypes metricTypes) {
        metric = metricTypes;
    }

    public MetricTypes getMetric() {
        return metric;
    }

    public void setMetric(MetricTypes metric) {
        this.metric = metric;
    }
}
