package com.crank.context;

import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.metricsystem.MetricTypes;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class MetricsSystemsContext {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public MetricSystem world() {
        return new MetricSystem(MetricTypes.World);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public MetricSystem us() {
        return new MetricSystem(MetricTypes.US);
    }
}
