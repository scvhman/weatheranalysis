package com.crank.location;

import com.crank.location.units.City;
import com.crank.location.units.Coordinates;
import com.crank.location.units.Country;
import com.crank.location.units.Location;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Iterator;

public class ReverserTest {

    Reverser reverser = new Reverser();
    Coordinates coordinates = new Coordinates(32.77, -96.79);
    City city = new City("Dallas");
    Country country = new Country("United States of America");
    Location Dallas = new Location(country, city);

    @Test
    public void testGetCoordinates() throws Exception {
        HashSet<Coordinates> coordinatesSet = reverser.getCoordinates(Dallas);
        Iterator<Coordinates> coordinatesIterator = coordinatesSet.iterator();
        Assert.assertEquals(coordinatesIterator.next(), coordinates);
    }

    @Test
    public void testGetLocation() throws Exception {
        Location location = reverser.getLocation(coordinates);
        country.setCountryCode("us");
        Assert.assertEquals(Dallas, location);
    }

}