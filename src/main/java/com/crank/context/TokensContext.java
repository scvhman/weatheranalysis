package com.crank.context;

import com.crank.codetemplates.SQLOrmTemplate;
import com.crank.weather.access.AccessToken;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class TokensContext {

    private AccessToken token;
    private SQLOrmTemplate sqlWorker = new SQLOrmTemplate();

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public AccessToken darkskytoken() {
        token = sqlWorker.getToken("darksky");
        return token;
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public AccessToken owmtoken() {
        token = sqlWorker.getToken("owm");
        return token;
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public AccessToken wutoken() {
        token = sqlWorker.getToken("wu");
        return token;
    }
}
