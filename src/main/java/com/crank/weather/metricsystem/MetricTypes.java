package com.crank.weather.metricsystem;

/**
 * Enum содержит список метрических систем
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public enum MetricTypes {

    US, World

}
