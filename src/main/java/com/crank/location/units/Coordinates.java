package com.crank.location.units;

import com.google.gson.annotations.SerializedName;

public class Coordinates {

    @SerializedName("lat")
    private double latitude;

    @SerializedName("lon")
    private double longitude;

    public Coordinates(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Coordinates() {
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String toString() {
        return String.valueOf(latitude) + String.valueOf(longitude);
    }

    public int hashCode() {
        return ((int) latitude * (int) longitude);
    }

    public boolean equals(Object o) {
        Coordinates coordinates = (Coordinates) o;
        if (this == o) {
            return true;
        } else if (o == null || (this.getClass() != o.getClass())) {
            return false;
        } else if (coordinates.getLatitude() == this.getLatitude() && coordinates.getLongitude() == this.getLongitude()) {
            return true;
        } else {
            return false;
        }
    }

}
