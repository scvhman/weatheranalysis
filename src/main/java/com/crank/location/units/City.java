package com.crank.location.units;

import com.google.gson.annotations.SerializedName;

public class City {

    @SerializedName(value = "city", alternate = {"village", "town", "hamlet"})
    private String name;

    public City(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
