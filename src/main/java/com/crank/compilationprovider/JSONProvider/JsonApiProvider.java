package com.crank.compilationprovider.JSONProvider;

import com.crank.location.units.Coordinates;
import com.crank.location.units.Location;
import com.crank.weather.units.Weather;

import java.util.ArrayList;

public interface JsonApiProvider {

    ArrayList<Weather> getWeatherList(Coordinates coordinates);

    ArrayList<Weather> getWeatherList(Location location);
}
