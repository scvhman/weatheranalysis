package com.crank.codetemplates;

import com.crank.weather.units.Weather;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Класс служит для создания нескольких часто используемых шаблонов кода связанных с парсингов JSON-объектов
 * СЛУЖИТ ДЛЯ ПРЕДОТВРАЩЕНИЯ ДУБЛИРОВАНИЯ КОДА
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class JsonParserTemplate {

    private JsonParser parser;

    /**
     * Метод возвращает JsonObject из строки с жсоном
     *
     * @param json - строка с json которую получает метод
     * @return object - jsonobject получаемый из строки json
     */
    public JsonObject getJsonObject(String json) {
        parser = new JsonParser();
        JsonObject object = parser.parse(json).getAsJsonObject();
        return object;
    }

    /** Метод возвращает объект класса Weather получаемых из json-объекта
     * @param element - передаваемый json объект
     * @return weather - Weather получаемый из объекта element
     */
    public Weather getWeatherFromJson(JsonElement element) {
        Gson gson = new Gson();
        Weather weather = gson.fromJson(element, Weather.class);
        return  weather;
    }

}
