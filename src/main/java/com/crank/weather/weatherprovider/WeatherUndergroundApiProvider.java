package com.crank.weather.weatherprovider;

import com.crank.codetemplates.JsonParserTemplate;
import com.crank.codetemplates.OkHTTPConnectionTemplate;
import com.crank.context.TemplatesContext;
import com.crank.context.TokensContext;
import com.crank.location.Reverser;
import com.crank.location.units.Coordinates;
import com.crank.location.units.Location;
import com.crank.weather.access.AccessToken;
import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.metricsystem.MetricTypes;
import com.crank.weather.units.Weather;
import com.google.gson.JsonObject;
import okhttp3.Response;
import org.joda.time.DateTime;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class WeatherUndergroundApiProvider implements WeatherProvider {

    private AccessToken token;
    private OkHTTPConnectionTemplate okHTTPConnectionTemplate;
    private JsonParserTemplate parserTemplate;
    private MetricSystem metricSystem;
    private String connection;
    private Response response;
    private JsonObject results;
    private Weather weather;

    public WeatherUndergroundApiProvider(MetricSystem metricSystem) {
        this.metricSystem = metricSystem;
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(TokensContext.class);
        token = (AccessToken) ctx.getBean("wutoken");
        ApplicationContext templatesContext =
                new AnnotationConfigApplicationContext(TemplatesContext.class);
        okHTTPConnectionTemplate = (OkHTTPConnectionTemplate) templatesContext.getBean("httpTemplate");
        parserTemplate = (JsonParserTemplate) templatesContext.getBean("jsonTemplate");
    }

    @Override
    public Weather getCurrentWeather(Coordinates coordinates) throws IOException {
        connection = "http://api.wunderground.com/api/" + token.getToken() + "/conditions/q/" + coordinates.getLatitude() + "," + coordinates.getLongitude() + ".json";
        response = okHTTPConnectionTemplate.getResponse(connection);
        results = parserTemplate.getJsonObject(response.body().string());
        weather = weatherundergroundJsonToWeather(results);
        return weather;
    }

    @Override
    public Weather getWeatherByTime(Coordinates coordinates, DateTime time) throws IOException {
        return null;
    }

    @Override
    public ArrayList<Weather> getCurrentWeather(Location location) {
        ArrayList<Weather> weathers = new ArrayList<>();
        Reverser reverser = new Reverser();
        HashSet<Coordinates> coordinates = new HashSet<>();
        try {
            coordinates = reverser.getCoordinates(location);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Iterator<Coordinates> iteratorCoordinates = coordinates.iterator();
        while (iteratorCoordinates.hasNext()) {
            Coordinates iteratorResult = iteratorCoordinates.next();
            connection = "http://api.wunderground.com/api/" + token.getToken() + "/conditions/q/" + iteratorResult.getLatitude() + "," + iteratorResult.getLongitude() + ".json";
            response = okHTTPConnectionTemplate.getResponse(connection);
            try {
                results = parserTemplate.getJsonObject(response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            weather = weatherundergroundJsonToWeather(results);
            weathers.add(weather);
        }
        return weathers;
    }

    @Override
    public ArrayList<Weather> getWeatherByTime(Location location, DateTime time) {
        return null;
    }

    @Override
    public ArrayList<Weather> getWeekForecast(Coordinates coordinates) {
        return null;
    }

    private Weather weatherundergroundJsonToWeather(JsonObject jsonResponse) {
        Weather weather = parserTemplate.getWeatherFromJson(jsonResponse.get("current_observation"));
        String humidity = results.get("current_observation").getAsJsonObject().get("relative_humidity").getAsString();
        BigDecimal humidityToNumber = new BigDecimal(humidity.trim().replace("%", ""));
        weather.setHumidity(humidityToNumber.doubleValue());
        if (metricSystem.getMetric() == MetricTypes.World) {
            weather.setTemperature((weather.getTemperature() - 32) * 0.5556);
            weather.setWindSpeed(1.609 * weather.getWindSpeed());
            weather.setVisibility(1.6 * weather.getVisibility());
        }
        return weather;
    }
}
