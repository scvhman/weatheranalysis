package com.crank.controllers;

import com.crank.location.units.Coordinates;
import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.metricsystem.MetricTypes;
import com.crank.weather.weatherprovider.DarkSkyApiProvider;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Index {

    @RequestMapping(value = "/")
    public ModelAndView indexPage() {
        DarkSkyApiProvider darkSkyApiProvider = new DarkSkyApiProvider(new MetricSystem(MetricTypes.US));
        darkSkyApiProvider.getWeekForecast(new Coordinates(32.7767, 96.7970));
        ModelAndView view = new ModelAndView("index");
        return view;
    }
}
