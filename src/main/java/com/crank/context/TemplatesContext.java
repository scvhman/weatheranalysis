package com.crank.context;

import com.crank.codetemplates.JsonParserTemplate;
import com.crank.codetemplates.OkHTTPConnectionTemplate;
import com.crank.codetemplates.SQLOrmTemplate;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class TemplatesContext {

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public OkHTTPConnectionTemplate httpTemplate() {
        return new OkHTTPConnectionTemplate();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public JsonParserTemplate jsonTemplate() {
        return new JsonParserTemplate();
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public SQLOrmTemplate sqlTemplate() {
        return new SQLOrmTemplate();
    }
}
