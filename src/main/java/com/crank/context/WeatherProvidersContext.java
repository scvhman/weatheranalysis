package com.crank.context;

import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.weatherprovider.DarkSkyApiProvider;
import com.crank.weather.weatherprovider.OpenWeatherMapApiProvider;
import com.crank.weather.weatherprovider.WeatherProvider;
import com.crank.weather.weatherprovider.WeatherUndergroundApiProvider;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class WeatherProvidersContext {

    private MetricSystem metricSystem;
    private ApplicationContext metricsContext =
            new AnnotationConfigApplicationContext(MetricsSystemsContext.class);

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public WeatherProvider darkskyWorld() {
        metricSystem = (MetricSystem) metricsContext.getBean("world");
        return new DarkSkyApiProvider(metricSystem);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public WeatherProvider darkskyUS() {
        metricSystem = (MetricSystem) metricsContext.getBean("us");
        return new DarkSkyApiProvider(metricSystem);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public WeatherProvider owmUS() {
        metricSystem = (MetricSystem) metricsContext.getBean("us");
        return new OpenWeatherMapApiProvider(metricSystem);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public WeatherProvider owmWorld() {
        metricSystem = (MetricSystem) metricsContext.getBean("world");
        return new OpenWeatherMapApiProvider(metricSystem);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public WeatherProvider weatherUndergroundWorld() {
        metricSystem = (MetricSystem) metricsContext.getBean("world");
        return new WeatherUndergroundApiProvider(metricSystem);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public WeatherProvider weatherUndergroundUS() {
        metricSystem = (MetricSystem) metricsContext.getBean("us");
        return new WeatherUndergroundApiProvider(metricSystem);
    }

}
