package com.crank.location.units;


import com.google.gson.annotations.SerializedName;

public class Country implements Comparable<Country> {

    @SerializedName("country")
    private String name;

    @SerializedName("country_code")
    private String countryCode;

    public Country(String name, String countryCode) {
        this.name = name;
        this.countryCode = countryCode;
    }

    public Country(String name) {
        this.name = name;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int compareTo(Country country) {
        if (this.name.equals(country.getName())) {
            return 1;
        } else {
            return 0;
        }
    }

    public String toString() {
        return name;
    }
}
