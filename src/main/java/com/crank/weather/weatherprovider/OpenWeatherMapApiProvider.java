package com.crank.weather.weatherprovider;

import com.crank.codetemplates.JsonParserTemplate;
import com.crank.codetemplates.OkHTTPConnectionTemplate;
import com.crank.context.TemplatesContext;
import com.crank.context.TokensContext;
import com.crank.location.units.Coordinates;
import com.crank.location.units.Location;
import com.crank.weather.access.AccessToken;
import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.metricsystem.MetricTypes;
import com.crank.weather.units.Weather;
import com.google.gson.JsonObject;
import okhttp3.Response;
import org.joda.time.DateTime;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.ArrayList;

public class OpenWeatherMapApiProvider implements WeatherProvider {

    private Response response;
    private JsonObject results;
    private AccessToken token;
    private OkHTTPConnectionTemplate okHttpProvider;
    private JsonParserTemplate parserTemplate;
    private MetricSystem metricSystem;
    private Weather weather;

    public OpenWeatherMapApiProvider(MetricSystem metricSystem) {
        this.metricSystem = metricSystem;
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(TokensContext.class);
        ApplicationContext templatesContext =
                new AnnotationConfigApplicationContext(TemplatesContext.class);
        okHttpProvider = (OkHTTPConnectionTemplate) templatesContext.getBean("httpTemplate");
        parserTemplate = (JsonParserTemplate) templatesContext.getBean("jsonTemplate");
        token = (AccessToken) ctx.getBean("owmtoken");
    }

    @Override
    public Weather getCurrentWeather(Coordinates coordinates) throws IOException {
        if (metricSystem.getMetric() == MetricTypes.US) {
            response = okHttpProvider.getResponse("http://api.openweathermap.org/data/2.5/weather?lat=" + coordinates.getLatitude() + "&lon=" + coordinates.getLongitude() + "&units=imperial" + "&appid=" + token.getToken());
        } else {
            response = okHttpProvider.getResponse("http://api.openweathermap.org/data/2.5/weather?lat=" + coordinates.getLatitude() + "&lon=" + coordinates.getLongitude() + "&units=metric" + "&appid=" + token.getToken());
        }
        results = parserTemplate.getJsonObject(response.body().string());
        weather = getOWMWeatherSimplified(results);
        //Не нашел альтернатив такому путю дополнения
        return weather;
    }

    @Override
    public Weather getWeatherByTime(Coordinates coordinates, DateTime time) throws IOException {
        return null;
    }

    @Override
    public ArrayList<Weather> getCurrentWeather(Location location) {
        if (metricSystem.getMetric() == MetricTypes.US) {
            response = okHttpProvider.getResponse("http://api.openweathermap.org/data/2.5/weather?q=" + location.getCity().getName()
                    + "," + location.getCountry().getCountryCode() + "&units=imperial" + "&appid=" + token.getToken());
        } else {
            response = okHttpProvider.getResponse("http://api.openweathermap.org/data/2.5/weather?q=" + location.getCity().getName()
                    + "," + location.getCountry().getCountryCode() + "&units=metric" + "&appid=" + token.getToken());

        }
        try {
            results = parserTemplate.getJsonObject(response.body().string());
        } catch (IOException e) {
            e.printStackTrace();
        }
        weather = getOWMWeatherSimplified(results);
        ArrayList<Weather> weatherArrayList = new ArrayList<>();
        weatherArrayList.add(weather);
        return weatherArrayList;
    }

    @Override
    public ArrayList<Weather> getWeatherByTime(Location location, DateTime time) {
        return null;
    }

    @Override
    public ArrayList<Weather> getWeekForecast(Coordinates coordinates) {
        return null;
    }

    private Weather getOWMWeatherSimplified(JsonObject element) {
        Weather weather = parserTemplate.getWeatherFromJson(element.get("main"));
        weather.setWindSpeed(element.get("wind").getAsJsonObject().get("speed").getAsDouble());
        weather.setSummary(element.get("weather").getAsJsonArray().get(0).getAsJsonObject().get("description").getAsString());
        return weather;
    }
}
