package com.crank.weather.weatherprovider;

import com.crank.codetemplates.JsonParserTemplate;
import com.crank.codetemplates.OkHTTPConnectionTemplate;
import com.crank.context.TemplatesContext;
import com.crank.context.TokensContext;
import com.crank.location.Reverser;
import com.crank.location.units.Coordinates;
import com.crank.location.units.Location;
import com.crank.weather.access.AccessToken;
import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.metricsystem.MetricTypes;
import com.crank.weather.units.Weather;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import okhttp3.Response;
import org.joda.time.DateTime;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Класс служит для получения погоды с DarkSky Api с помощью JSON
 *
 * @author scvhabodka-man
 * @version 1.0
 */

public class DarkSkyApiProvider implements WeatherProvider {

    private MetricSystem metricSystem;
    private AccessToken token;
    private Weather weather;
    private OkHTTPConnectionTemplate connectionTemplate;
    private JsonParserTemplate jsonTemplate;
    private ArrayList<Weather> weatherSet = new ArrayList<>();

    public DarkSkyApiProvider(MetricSystem metricSystem) {
        this.metricSystem = metricSystem;
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(TokensContext.class);
        ApplicationContext templatesContext =
                new AnnotationConfigApplicationContext(TemplatesContext.class);
        token = (AccessToken) ctx.getBean("darkskytoken");
        connectionTemplate = (OkHTTPConnectionTemplate) templatesContext.getBean("httpTemplate");
        jsonTemplate = (JsonParserTemplate) templatesContext.getBean("jsonTemplate");
    }

    /**
     * Данный метод получает ТЕКУЩУЮ погоду через апи по координатам.
     * Стучит в getWeatherByTime передавая ему текущую дату(datetime - через либу jodatime)
     *
     * @param coordinates - объект с координатами места для которого нужно получать погоду
     * @return weather - объект погоды который вовращается
     */
    @Override
    public Weather getCurrentWeather(Coordinates coordinates) throws IOException {
        DateTime time = new DateTime();
        weather = getWeatherByTime(coordinates, time);
        return weather;
    }

    /**
     * Данный метод получает погоду в любое время по координатам через апи.
     * Стучит в getWeatherByTime передавая ему текущую дату(datetime - через либу jodatime)
     *
     * @param coordinates - объект с координатами места для которого нужно получать погоду
     * @param time        - передаваемый объект DateTime(библиотека jodatime) с нужным временем
     * @return weather - объект погоды который вовращается
     */
    @Override
    public Weather getWeatherByTime(Coordinates coordinates, DateTime time) throws IOException {
        String request;
        if (metricSystem.getMetric() == MetricTypes.US) { //делю на 1000 потому что нужно чтобы получить текущее юникс время
            request = "https://api.darksky.net/forecast/" + token.getToken() + "/" + coordinates.getLatitude() + ","
                    + coordinates.getLongitude() + "," + time.getMillis() / 1000 + "?units=us";
        } else {
            request = "https://api.darksky.net/forecast/" + token.getToken() + "/" + coordinates.getLatitude() + ","
                    + coordinates.getLongitude() + "," + time.getMillis() / 1000 + "?units=si";
        }
        Response response = connectionTemplate.getResponse(request);
        JsonElement results = jsonTemplate.getJsonObject(response.body().string());
        weather = jsonTemplate.getWeatherFromJson(results.getAsJsonObject().get("currently"));
        return weather;
    }

    /**
     * Данный метод получает погоду в любое время по локации через апи.
     * ВОЗВРАЩАЕТ ARRAYLIST, А НЕ WEATHER Т.К. МОЖЕТ СУЩЕСТВОВАТЬ НЕСКОЛЬКО МЕСТ С ОДИНАКОВЫМИ НАЗВАНИЯМИ,
     * в отличии от координат которые одни на одно место.
     * Стучит в getWeatherByTime передавая ему текущую дату(datetime - через либу jodatime)
     *
     * @param location - объект с локацией
     * @return weather - объект погоды который вовращается
     */
    @Override
    public ArrayList<Weather> getCurrentWeather(Location location) {
        DateTime dateTime = new DateTime();
        weatherSet = getWeatherByTime(location, dateTime);
        return weatherSet;
    }

    /**
     * Данный метод получает погоду в любое время по локации через апи.
     * ВОЗВРАЩАЕТ ARRAYLIST, А НЕ WEATHER Т.К. МОЖЕТ СУЩЕСТВОВАТЬ НЕСКОЛЬКО МЕСТ С ОДИНАКОВЫМИ НАЗВАНИЯМИ,
     * в отличии от координат которые одни на одно место.
     * Стучит в getWeatherByTime передавая ему текущую дату(datetime - через либу jodatime)
     *
     * @param location - объект с локацией
     * @param time     - передаваемый объект DateTime(библиотека jodatime) с нужным временем
     * @return weather - объект погоды который вовращается
     */
    @Override
    public ArrayList<Weather> getWeatherByTime(Location location, DateTime time) {
        String request;
        Reverser reverser = new Reverser();
        HashSet<Coordinates> coordinatesSet = new HashSet<>();
        try {
            coordinatesSet = reverser.getCoordinates(location);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Iterator<Coordinates> coordinatesIterator = coordinatesSet.iterator();
        while (coordinatesIterator.hasNext()) {
            Coordinates coordinates = coordinatesIterator.next();
            if (metricSystem.getMetric() == MetricTypes.US) { //делю на 1000 потому что нужно чтобы получить текущее юникс время
                request = "https://api.darksky.net/forecast/" + token.getToken() + "/" + coordinates.getLatitude() + "," + coordinates.getLongitude() + "," + time.getMillis() / 1000 + "?units=us";
            } else {
                request = "https://api.darksky.net/forecast/" + token.getToken() + "/" + coordinates.getLatitude() + "," + coordinates.getLongitude() + "," + time.getMillis() / 1000 + "?units=si";
            }
            try {
                Response response = connectionTemplate.getResponse(request);
                JsonElement results = jsonTemplate.getJsonObject(response.body().string());
                weather = jsonTemplate.getWeatherFromJson(results.getAsJsonObject().get("currently"));
                weatherSet.add(weather);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return weatherSet;
    }

    @Override
    public ArrayList<Weather> getWeekForecast(Coordinates coordinates) {
        String request;
        if (metricSystem.getMetric() == MetricTypes.US) { //делю на 1000 потому что нужно чтобы получить текущее юникс время
            request = "https://api.darksky.net/forecast/" + token.getToken() + "/" + coordinates.getLatitude() + ","
                    + coordinates.getLongitude() + "?units=us";
        } else {
            request = "https://api.darksky.net/forecast/" + token.getToken() + "/" + coordinates.getLatitude() + ","
                    + coordinates.getLongitude() + "?units=si";
        }
        Response response = connectionTemplate.getResponse(request);
        JsonArray jsonResponse;
        try {
            jsonResponse = jsonTemplate.getJsonObject(response.body().string()).
                    get("daily").getAsJsonObject().get("data").getAsJsonArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
