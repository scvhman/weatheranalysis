package com.crank.codetemplates;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Класс служит для создания нескольких часто используемых шаблонов кода связанных с использованием okhttp
 * СЛУЖИТ ДЛЯ ПРЕДОТВРАЩЕНИЯ ДУБЛИРОВАНИЯ КОДА
 *
 * @author scvhabodka-man
 * @version 1.0
 */
public class OkHTTPConnectionTemplate {

    private Response response;

    /**
     * Метод возвращает JsonObject из строки с жсоном
     *
     * @param request - строчка по которой необходимо сделать реквест
     * @return response - объект с ответом получаемым от сервера
     */
    public Response getResponse(String request) {
        OkHttpClient client = new OkHttpClient();
        Request httpRequest = new Request.Builder().url(request).header("User-Agent", "crank.io").build();
        try {
            response = client.newCall(httpRequest).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }
}
