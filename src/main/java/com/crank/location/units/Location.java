package com.crank.location.units;

public class Location {

    private Country country;
    private City city;

    public Location(Country country, City city) {
        this.country = country;
        this.city = city;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }


    public int hashCode() {
        return country.getName().hashCode() + city.getName().hashCode();
    }

    public boolean equals(Object o) {
        Location location = (Location) o;
        if (this == o) {
            return true;
        } else if (o == null || (this.getClass() != o.getClass())) {
            return false;
        } else
            return location.country.getName().equals(this.country.getName()) && location.city.getName().equals(this.city.getName());
    }
}
