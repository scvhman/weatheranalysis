package com.crank.compilationprovider.JSONProvider;

import com.crank.context.WeatherProvidersContext;
import com.crank.location.units.Coordinates;
import com.crank.location.units.Location;
import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.metricsystem.MetricTypes;
import com.crank.weather.units.Weather;
import com.crank.weather.weatherprovider.DarkSkyApiProvider;
import com.crank.weather.weatherprovider.OpenWeatherMapApiProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.util.ArrayList;

public class JsonNowProvider implements JsonApiProvider {


    public JsonNowProvider(MetricSystem metricSystem) {
        ApplicationContext providerContext =
                new AnnotationConfigApplicationContext(WeatherProvidersContext.class);
        if (metricSystem.getMetric() == MetricTypes.US) {
            darkSkyApiProvider = (DarkSkyApiProvider) providerContext.getBean("darkskyUS");
            openWeatherMapApiProvider = (OpenWeatherMapApiProvider) providerContext.getBean("owmUS");
        } else {
            darkSkyApiProvider = (DarkSkyApiProvider) providerContext.getBean("darkskyWorld");
            openWeatherMapApiProvider = (OpenWeatherMapApiProvider) providerContext.getBean("owmWorld");
        }
    }

    private DarkSkyApiProvider darkSkyApiProvider;
    private OpenWeatherMapApiProvider openWeatherMapApiProvider;
    private ArrayList<Weather> compilation = new ArrayList<>();

    @Override
    public ArrayList<Weather> getWeatherList(Coordinates coordinates) {
        try {
            Weather darkSky = darkSkyApiProvider.getCurrentWeather(coordinates);
            Weather owm = openWeatherMapApiProvider.getCurrentWeather(coordinates);
            compilation.add(darkSky);
            compilation.add(owm);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return compilation;
    }

    @Override
    public ArrayList<Weather> getWeatherList(Location location) {
        ArrayList<Weather> darkSky = darkSkyApiProvider.getCurrentWeather(location);
        ArrayList<Weather> owm = openWeatherMapApiProvider.getCurrentWeather(location);
        compilation.addAll(darkSky);
        compilation.addAll(owm);
        return compilation;
    }
}
