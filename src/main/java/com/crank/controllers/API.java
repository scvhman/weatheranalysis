package com.crank.controllers;

import com.crank.compilationprovider.JSONProvider.JsonApiProvider;
import com.crank.compilationprovider.JSONProvider.JsonTimeMachineProvider;
import com.crank.context.JsonProvidersContext;
import com.crank.context.MetricsSystemsContext;
import com.crank.location.units.City;
import com.crank.location.units.Coordinates;
import com.crank.location.units.Country;
import com.crank.location.units.Location;
import com.crank.weather.metricsystem.MetricSystem;
import com.crank.weather.units.Weather;
import org.joda.time.DateTime;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * Контроллер отвечающий за REST api сайта
 *
 * @author scvhabodka-man
 * @version 1.0
 */
@RestController
@RequestMapping(value = "/api")
public class API {

    private JsonApiProvider facade;
    private ApplicationContext context =
            new AnnotationConfigApplicationContext(JsonProvidersContext.class);
    private ApplicationContext metricsContext =
            new AnnotationConfigApplicationContext(MetricsSystemsContext.class);

    /**
     * Метод отвечающий за получение погоды
     */
    @RequestMapping(value = "/weather", headers = "Accept=application/json", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> getWeatherlist(@RequestParam(value = "lat", required = false) Double lat,
                                            @RequestParam(value = "lon", required = false) Double lon,
                                            @RequestParam(value = "country", required = false) String country,
                                            @RequestParam(value = "city", required = false) String city,
                                            @RequestParam(value = "metrics", required = false, defaultValue = "world") String metrics,
                                            @RequestParam(value = "time", required = false) Long time) {
        if (time == null) {
            if (metrics.equals("us")) {
                facade = (JsonApiProvider) context.getBean("nowStateUS");
            } else {
                facade = (JsonApiProvider) context.getBean("nowStateWorld");
            }
        } else {
            if (metrics.equals("us")) {
                MetricSystem metricSystem = (MetricSystem) metricsContext.getBean("us");
                facade = new JsonTimeMachineProvider(metricSystem, new DateTime(time * 1000));
            } else {
                MetricSystem metricSystem = (MetricSystem) metricsContext.getBean("world");
                facade = new JsonTimeMachineProvider(metricSystem, new DateTime(time * 1000));
            }
        }

        if (lat == null && lon == null && country == null && city == null) {
            return new ResponseEntity<>("Bad request", HttpStatus.BAD_REQUEST);
        } else if (lat == null || lon == null) {
            Location location = new Location(new Country(country), new City(city));
            ArrayList<Weather> weathers = facade.getWeatherList(location);
            return new ResponseEntity<>(weathers, HttpStatus.OK);
        } else {
            Coordinates coordinates = new Coordinates(lat, lon);
            ArrayList<Weather> weathers = facade.getWeatherList(coordinates);
            return new ResponseEntity<>(weathers, HttpStatus.OK);
        }
    }

}
